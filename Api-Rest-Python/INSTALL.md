1. Start database:
In db
docker build -t netflix_titles ./
docker run -d --name netflix_titles_container -p 5432:5432 netflix_titles

2. Start backend
In module 2
Install python dependencies
python -m app

3. Start frontend
In module 1
Install python dependencies
python -m ingest_titles PathToCSV

BONUS: in module 1, 
python -m movies --list
python -m movies --title Garbage

http://localhost:8080/swagger/#/
http://localhost:8080/swagger.json
