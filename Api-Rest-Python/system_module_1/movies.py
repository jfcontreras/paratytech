# python -m movies --list
# python -m movies --title Garbage

import requests
import argparse

BASE_URL = 'http://127.0.0.1:8080' 

def list_netflix_titles():
    try:
        response = requests.get(f'{BASE_URL}/netflix/titles')
        if response.status_code == 200:
            titles = response.json().get('titles')
            if titles:
                for title in titles:
                    print(f"Title: {title['title']}")
                    print(f"Type: {title['type']}")
                    print(f"Release Year: {title['release_year']}")
                    print()
            else:
                print("No titles found.")
        else:
            print(f"Error: {response.status_code} - {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")

def get_description(title_to_search):
    try:
        response = requests.get(f'{BASE_URL}/netflix/titles')
        if response.status_code == 200:
            titles = response.json().get('titles')
            if titles:
                found = False
                for title in titles:
                    if title['title'].lower() == title_to_search.lower():
                        print(f"Title: {title['title']}")
                        print(f"Description: {title['description']}")
                        found = True
                        break
                if not found:
                    print(f"No title found: {title_to_search}")
            else:
                print("No titles found.")
        else:
            print(f"Error: {response.status_code} - {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Query titles")
    parser.add_argument("--list", action="store_true", help="List all titles")
    parser.add_argument("--title", help="Search")
    args = parser.parse_args()

    if args.list:
        list_netflix_titles()
    elif args.title:
        get_description(args.title)
    else:
        print("Use '--list' to list all titles or '--title' to search.")
