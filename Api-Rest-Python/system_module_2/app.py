# python -m app

from flask import Flask, request, jsonify
from flask_restx import Resource, Api, Namespace, fields
from flask_swagger_ui import get_swaggerui_blueprint
import psycopg2
import os
import json
import logging
import chardet

# Define app
app = Flask(__name__)
api = Api(app)
ns = api.namespace('netflix', description='Netflix titles')

# Create directory for logs
log_dir = 'logs'
os.makedirs(log_dir, exist_ok=True)

# Configure logging for duplicate records
logging.basicConfig(
    encoding='utf-8',
    filename=os.path.join(log_dir, 'error.log'),
    level=logging.DEBUG,
    format='%(asctime)s - %(message)s'
)

# Define DB connection
DB_HOST = 'localhost'
DB_PORT = '5432'
DB_NAME = 'netflix_titles'
DB_USER = 'docker'
DB_PASSWORD = 'docker'

# Initialize connection with UTF-8 encoding
try:
    conn = psycopg2.connect(
        host=DB_HOST,
        port=DB_PORT,
        database=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        client_encoding='utf8'
    )
except psycopg2.Error as e:
    logging.error(f"Failed to connect to the database: {e}")

# Define the Netflix title data model
netflix_title_model = api.model('NetflixTitle', {
    'show_id': fields.String(required=True, description='ID'),
    'type': fields.String(description='Movies/Shows'),
    'title': fields.String(description='Title'),
    'director': fields.String(description='Director'),
    'cast': fields.String(description='Cast'),
    'country': fields.String(description='Country'),
    'date_added': fields.String(description='Date added'),
    'release_year': fields.String(description='Release year'),
    'rating': fields.String(description='Rating'),
    'duration': fields.String(description='Duration'),
    'listed_in': fields.String(description='Categories'),
    'description': fields.String(description='Description')
})

def safe_encode(string):
    return string.encode('utf-8', errors='ignore').decode('utf-8')

# Execute SQL queries
def execute_query(query, params):
    #logging.info("Querying for:", params)
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, *params)
            conn.commit()
    except psycopg2.Error as e:
        logging.error(f"Error executing query: {e}")

# Routes
@ns.route('/titles')
class NetflixTitleResource(Resource):

    # Get all titles
    @ns.doc('list_titles')
    def get(self):
        try:
            with conn.cursor() as cursor:
                cursor.execute("SELECT * FROM netflix_titles")
                titles = [dict(zip([desc[0] for desc in cursor.description], row)) for row in cursor.fetchall()]
                return {'titles': titles}
        except Exception as e:
            logging.error(f"An error occurred: {e}")
            return {'message': 'An error occurred.'}, 500

    # Insert new titles
    @ns.doc('create_title')
    @ns.expect([netflix_title_model], validate=True)
    def post(self):
        try:
            json_raw = request.get_data().decode('utf-8')

            if not json_raw:
                logging.error("Empty input")
                return {'message': 'Empty input data.'}, 400

            json_data = json.loads(json_raw)

            if not isinstance(json_data, list):
                error_message = 'Invalid input format.'
                logging.error(error_message)
                return {'message': error_message}, 400

            imported_records = []

            for new_title in json_data:

                logging.info("")

                # Check import against db for dupes
                query = "SELECT show_id FROM netflix_titles WHERE show_id = %s"
                params = (safe_encode(new_title.get('show_id')),)

                with conn.cursor() as cursor:
                    cursor.execute(query, params)
                    existing_record = cursor.fetchone()

                    if existing_record:
                        # Log duplicates
                        logging.info(f'Duplicate record: {json.dumps(new_title)}')
                    else:
                        # Insert
                        query = """
                            INSERT INTO netflix_titles (show_id, type, title, director, "cast", country, date_added, release_year, rating, duration, listed_in, description)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        """
                        params = (
                            new_title.get('show_id'), new_title.get('type'), new_title.get('title'),
                            new_title.get('director'), new_title.get('cast'), new_title.get('country'),
                            new_title.get('date_added'), new_title.get('release_year'), new_title.get('rating'),
                            new_title.get('duration'), new_title.get('listed_in'), new_title.get('description')
                        )
                        execute_query(query, *params)
                        imported_records.append(new_title)

            if imported_records:
                return {'message': 'Records were imported.'}, 201
            else:
                return {'message': 'All records were duplicates or had errors.'}, 200
        except Exception as e:
            error_message = f"An error occurred: {str(e)}"
            logging.error(error_message)
            return {'message': 'An error occurred while processing the request.'}, 500

# 404
@app.errorhandler(404)
def not_found(error):
    return {'message': 'Resource not found'}, 404

# 500
@app.errorhandler(500)
def internal_server_error(error):
    return {'message': 'Internal server error'}, 500

# Swagger
SWAGGER_URL = '/swagger'
API_URL = '/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={'app_name': 'Netflix titles'}
)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

# Run
if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)
