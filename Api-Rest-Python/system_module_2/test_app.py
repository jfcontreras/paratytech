# basic unit testing for the Flask backend
# run with python -m unittest test_app

import unittest
from app import app
from flask_testing import TestCase

class AppTestCase(TestCase):

    def create_app(self):
        app.config['TESTING'] = True
        return app

    def test_insert_title(self):
        # Test data
        new_title = [
            {
                'show_id': 's9999',
                'type': 'Movie',
                'title': 'Test Movie',
                'director': 'John Doe',
                'cast': 'John, Jane',
                'country': 'USA',
                'date_added': '2023-09-15',
                'release_year': '2023',
                'rating': 'TV-MA',
                'duration': '120 min',
                'listed_in': 'Action',
                'description': 'A test movie.'
            }
        ]

        # POST query
        response = self.client.post('/netflix/titles', json=new_title)

        # Ensure HTTP 201 for new or 200 for existing
        self.assertIn(response.status_code, [200,201])

if __name__ == '__main__':
    unittest.main()
