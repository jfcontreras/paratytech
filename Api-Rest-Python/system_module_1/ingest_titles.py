# python -m ingest_titles PATH_TO_FILE

import pandas as pd
import requests
import json
import argparse
import sys
import io
import chardet 

# Encode console output as UTF-8
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8')

# Parse the command-line argument
parser = argparse.ArgumentParser(description='Import CSV into Netflix titles database')
parser.add_argument('csv_file_path', type=str, help='Path to CSV file')
args = parser.parse_args()

# Receive the provided CSV file
csv_file_path = args.csv_file_path
print("Attempting to import", csv_file_path, "to the Netflix titles database...")

# Define URL for backend
api_url = 'http://localhost:8080/netflix/titles'

# Ensure the JSON conforms to the expected database structure
def validate_json_structure(data):
    expected_fields = [
        "show_id", "type", "title", "director", "cast", "country",
        "date_added", "release_year", "rating", "duration", "listed_in", "description"
    ]
    for record in data:
        for field in expected_fields:
            if field not in record:
                return False
    return True

# Detect CSV file encoding using chardet
with open(csv_file_path, 'rb') as f:
    result = chardet.detect(f.read())
encoding = result['encoding']
print("Input is encoded as:",encoding)

# Chunk size for processing
chunk_size = 500  

# Read and process the CSV in chunks with detected encoding
chunks = pd.read_csv(csv_file_path, encoding=encoding, chunksize=chunk_size)
for chunk in chunks:
    chunk = chunk.where(pd.notnull(chunk), None)
    chunk = chunk.apply(lambda x: x.astype(str))

    # Create JSON of records to import
    chunk = chunk.fillna("")
    json_data = json.loads(chunk.to_json(orient='records', force_ascii=False))

    # Validate the JSON structure
    if not validate_json_structure(json_data):
        print("Invalid JSON structure.")
    else:
        # Encode data as UTF-8
        json_str = json.dumps(json_data, ensure_ascii=False).encode('utf-8')
        headers = {'Content-Type': 'application/json; charset=utf-8'}

        print(headers)
        print(json_str)

        try:
            response = requests.post(api_url, data=json_str, headers=headers)

            # Check the response
            if response.status_code == 201:
                print('Loaded new titles successfully.')
            elif response.status_code == 400:
                print('Please correct input file encoding.')
            else:
                print('Data load problem, possible duplicates in DB.')
                print('Response:', response)
        except requests.exceptions.RequestException as e:
            print('An error occurred while sending the request:', str(e))