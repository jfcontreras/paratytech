CREATE TABLE "netflix_titles"
(
    "id" SERIAL PRIMARY KEY,
    "show_id" VARCHAR NOT NULL,
    "type" VARCHAR NULL,
    "title" VARCHAR NULL,
    "director" VARCHAR NULL,
    "cast" VARCHAR NULL,
    "country" VARCHAR NULL,
    "date_added" VARCHAR NULL,
    "release_year" VARCHAR NULL,
    "rating" VARCHAR NULL,
    "duration" VARCHAR NULL,
    "listed_in" VARCHAR NULL,
    "description" VARCHAR NULL
);